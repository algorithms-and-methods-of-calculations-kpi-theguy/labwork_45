package com.kpi.yuriy.labwork45.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.PointsGraphSeries;
import com.kpi.yuriy.labwork45.NewtonsMethod;
import com.kpi.yuriy.labwork45.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class NonLinearGraph extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private TextView textView_graph;
    private GraphView graph_non_lin;

    private FloatingActionButton fab;
    private FloatingActionButton fab_share;

    private double XRoot;
    private double XRoot2;
    private int num_points;

    private int minValue;
    private int maxValue;

    private EditText editText_number_points;
    private View alertView_dialog;

    private NewtonsMethod nm;

    public NonLinearGraph() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_non_linear_graph, container, false);
        textView_graph = (TextView) v.findViewById(R.id.textView_graph);
        graph_non_lin = (GraphView) v.findViewById(R.id.graph_non_lin);
        fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        fab_share = (FloatingActionButton) getActivity().findViewById(R.id.fab_share);
        Bundle args = getArguments();
        if(args == null){
            textView_graph.setText(R.string.nothing_build);
        }
        else{
            textView_graph.setText(R.string.enter_the_number_of_points);
            fab.setVisibility(View.VISIBLE);
            XRoot = args.getDouble("XArgument");
            XRoot2 = args.getDouble("X2Argument");
            minValue = args.getInt("min");
            maxValue = args.getInt("max");
            nm = new NewtonsMethod();
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createDialogNumbers();
            }
        });
        fab_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File fPath = Environment.getExternalStorageDirectory();
                File f = new File(fPath + "/data/graph_non.png");
                Bitmap returnedBitmap = nm.getBipmapFromGraph(graph_non_lin);
                try {
                    if (!f.exists()) {
                        f.createNewFile();
                    }
                    FileOutputStream os = new FileOutputStream(f);
                    returnedBitmap.compress(Bitmap.CompressFormat.PNG, 80, os);
                    os.close();
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("image/png");
                    Uri uri = Uri.parse("file://"+ Environment.getExternalStorageDirectory()+"/data/graph_non.png");
                    intent.putExtra(Intent.EXTRA_STREAM, uri);
                    startActivity(Intent.createChooser(intent, "Share via"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        return v;
    }

    public void createDialogNumbers(){
        LayoutInflater inflater = getActivity().getLayoutInflater();
        alertView_dialog = inflater.inflate(R.layout.dialog_2_0_, null);
        editText_number_points = (EditText) alertView_dialog.findViewById(R.id.editText_number_points);
        AlertDialog.Builder ad = new AlertDialog.Builder(getActivity());
        ad.setView(alertView_dialog);
        ad.setTitle("Graph points");
        ad.setMessage("Enter the number of points in graph:");
        ad.setPositiveButton("Set number", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                textView_graph.setText(R.string.graph_build);
                num_points = Integer.parseInt(editText_number_points.getText().toString());
                fab_share.setVisibility(View.VISIBLE);
                DataPoint[] functionS = new DataPoint[num_points];
                double h = (double) (maxValue - minValue) / num_points;
                for (int i = 0; i < num_points; i++) {
                    functionS[i] = new DataPoint(minValue + i * h, nm.function(minValue + i * h));
                }
                LineGraphSeries<DataPoint> ls1 = new LineGraphSeries<>(functionS);
                ls1.setTitle("f(x)");
                ls1.setColor(Color.RED);
                ls1.setThickness(8);
                graph_non_lin.setVisibility(View.VISIBLE);
                graph_non_lin.addSeries(ls1);
                graph_non_lin.getViewport().setScalable(true);
                graph_non_lin.getLegendRenderer().setVisible(true);
                graph_non_lin.getLegendRenderer().setBackgroundColor(Color.argb(0x80, 0x80, 0x80, 0xff));
                DataPoint[] xRoot;
                if (XRoot2 != 0) {
                    xRoot = new DataPoint[2];
                    xRoot[0] = new DataPoint(XRoot, 0);
                    xRoot[1] = new DataPoint(XRoot2, 0);
                }
                else{
                    xRoot = new DataPoint[1];
                    xRoot[0] = new DataPoint(XRoot, 0);
                }
                PointsGraphSeries<DataPoint> ps = new PointsGraphSeries<>(xRoot);
                ps.setTitle("xRoot");
                ps.setColor(Color.GREEN);
                ps.setSize(30);
                graph_non_lin.addSeries(ps);
            }
        });
        ad.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getActivity(), "The Graphic was not built!", Toast.LENGTH_SHORT).show();
            }
        });
        AlertDialog a = ad.create();
        a.show();
    }

    @Override
    public void onDestroyView() {
        fab.setVisibility(View.GONE);
        fab_share.setVisibility(View.GONE);
        super.onDestroyView();
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

}
