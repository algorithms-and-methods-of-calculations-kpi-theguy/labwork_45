package com.kpi.yuriy.labwork45.fragments;

import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.kpi.yuriy.labwork45.NewtonsMethod;
import com.kpi.yuriy.labwork45.R;


public class NonLinearCalc extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private EditText editText_non_acc;
    private CrystalRangeSeekbar range_bar;

    private LinearLayout lin_res_1;
    private LinearLayout lin_res_2;
    private LinearLayout lin_res_1a;

    private TextView textView_minBound;
    private TextView textView_maxBound;
    private TextView textView_non_res;

    private EditText editText_non_res;
    private EditText editText_non_res2;
    private EditText editText_non_res3;

    private Button button_non_calc;

    private int minValueBar;
    private int maxValueBar;

    private double[] res;

    private NewtonsMethod nm;

    private OnFragmentInteractionListener mListener;

    public NonLinearCalc() {

    }


    public static NonLinearCalc newInstance(String param1, String param2) {
        NonLinearCalc fragment = new NonLinearCalc();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_non_linear_calc, container, false);
        editText_non_acc = (EditText) v.findViewById(R.id.editText_non_acc);
        range_bar = (CrystalRangeSeekbar) v.findViewById(R.id.range_bar);
        editText_non_res = (EditText) v.findViewById(R.id.editText_non_res);
        editText_non_res2 = (EditText) v.findViewById(R.id.editText_non_res2);
        editText_non_res3 = (EditText) v.findViewById(R.id.editText_non_res3);
        textView_minBound = (TextView) v.findViewById(R.id.textView_minBound);
        textView_maxBound = (TextView) v.findViewById(R.id.textView_maxBound);
        lin_res_1 = (LinearLayout) v.findViewById(R.id.lin_res_1);
        lin_res_2 = (LinearLayout) v.findViewById(R.id.lin_res_2);
        lin_res_1a = (LinearLayout) v.findViewById(R.id.lin_res_1a);
        button_non_calc = (Button) v.findViewById(R.id.button_non_calc);
        textView_non_res = (TextView) v.findViewById(R.id.textView_non_res);
        editText_non_res.setKeyListener(null);
        editText_non_res2.setKeyListener(null);
        editText_non_res3.setKeyListener(null);

        range_bar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                minValueBar = Integer.parseInt(String.valueOf(minValue));
                maxValueBar = Integer.parseInt(String.valueOf(maxValue));
                textView_minBound.setText(String.valueOf(minValue));
                textView_maxBound.setText(String.valueOf(maxValue));
            }
        });

        button_non_calc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double accur = Double.parseDouble(editText_non_acc.getText().toString());
                if (editText_non_acc.getText().toString().isEmpty()){
                    Toast.makeText(getActivity(), "Enter accuracy of calculation firstly!", Toast.LENGTH_SHORT).show();
                }
                nm = new NewtonsMethod();
                nm.setE(accur);
                lin_res_1.setVisibility(View.VISIBLE);
                lin_res_2.setVisibility(View.VISIBLE);
                textView_non_res.setVisibility(View.VISIBLE);
                double res = 0;
                double res2 = 0;
                if (nm.function(minValueBar) * nm.function(maxValueBar) > 0) {
                    if ((minValueBar < 0 && maxValueBar < 0) || (minValueBar > 0 && maxValueBar > 0)){
                        Toast.makeText(getActivity(), "You entered bad values!", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        double buff = (minValueBar + maxValueBar)/2;
                        res = nm.XCalculation(minValueBar, buff);
                        res2 = nm.XCalculation(buff, maxValueBar);
                    }
                }
                else{
                    res = nm.XCalculation(minValueBar, maxValueBar);
                    res2 = 0;
                    lin_res_1a.setVisibility(View.GONE);
                }
                int rez = nm.getNumIter();
                editText_non_res.setText(String.valueOf(res));
                editText_non_res2.setText(String.valueOf(rez));
                if (res2 != 0){
                    lin_res_1a.setVisibility(View.VISIBLE);
                    editText_non_res3.setText(String.valueOf(res2));
                }
                mListener.onXCalculation(res, res2, accur, rez, minValueBar, maxValueBar);
            }
        });
        return v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onXCalculation(double x,double x2, double acc, int iter, int min, int max);
    }
}
