package com.kpi.yuriy.labwork45.fragments;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crystal.crystalrangeseekbar.interfaces.OnSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalSeekbar;
import com.kpi.yuriy.labwork45.GaussianMethod;
import com.kpi.yuriy.labwork45.R;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.Random;

public class SystemLinearCalc extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private TextView textView_sys_equ;

    private LinearLayout linear_eXs;
    private TextView textView_X1;
    private TextView textView_X2;
    private CrystalSeekbar seekBar_el;

    private LinearLayout linear_sys_check;
    private CheckBox checkBox_sys_check;

    private Button button_sys_create;

    private LinearLayout linear_system;

    private GridLayout table_sys_coef;
    private Button button_sys_calc;

    private TextView textView_sys_res;
    private GridLayout table_sys_res;

    private LinearLayout[][] cells;
    private LinearLayout[][] cells2;
    private EditText[][] values;
    private EditText[] values2;

    private GaussianMethod gm;

    private int progress;
    private double[] XRoots;

    private OnFragmentInteractionListener mListener;

    public SystemLinearCalc() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_system_linear_calc, container, false);
        textView_sys_equ = (TextView) v.findViewById(R.id.textView_sys_equ);
        linear_eXs = (LinearLayout) v.findViewById(R.id.linear_eXs);
        textView_X1 = (TextView) v.findViewById(R.id.textView_X1);
        textView_X2 = (TextView) v.findViewById(R.id.textView_X2);
        seekBar_el = (CrystalSeekbar) v.findViewById(R.id.seekBar_el);
        button_sys_create = (Button) v.findViewById(R.id.button_sys_create);
        linear_system = (LinearLayout) v.findViewById(R.id.linear_system);
        table_sys_coef  = (GridLayout) v.findViewById(R.id.table_sys_coef);
        button_sys_calc = (Button) v.findViewById(R.id.button_sys_calc);
        textView_sys_res = (TextView) v.findViewById(R.id.textView_sys_res);
        table_sys_res = (GridLayout) v.findViewById(R.id.table_sys_res);
        linear_sys_check = (LinearLayout) v.findViewById(R.id.linear_sys_check);
        checkBox_sys_check = (CheckBox) v.findViewById(R.id.checkBox_sys_check);
        checkBox_sys_check.toggle();

        textView_X1.setText(String.valueOf(2));
        seekBar_el.setOnSeekbarChangeListener(new OnSeekbarChangeListener() {
            @Override
            public void valueChanged(Number value) {
                progress = Integer.parseInt(String.valueOf(value));
                textView_X2.setText(String.valueOf(progress));
            }
        });
        button_sys_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textView_sys_equ.setVisibility(View.GONE);
                linear_eXs.setVisibility(View.GONE);
                seekBar_el.setVisibility(View.GONE);
                button_sys_create.setVisibility(View.GONE);
                linear_sys_check.setVisibility(View.GONE);
                linear_system.setVisibility(View.VISIBLE);
                generateTable(progress, checkBox_sys_check.isChecked());
            }
        });
        button_sys_calc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double[][] buff1 = new double[progress][progress];
                double[] buff2 = new double[progress];
                XRoots = new double[progress];
                for (int i = 0; i < progress; i++){
                    for (int j = 0; j < progress; j++){
                        buff1[i][j] = Double.parseDouble(values[i][j].getText().toString());
                    }
                    buff2[i] = Double.parseDouble(values[i][progress].getText().toString());
                }
                gm = new GaussianMethod(progress);
                XRoots = gm.CalculationOfSystem(buff1, buff2);
                textView_sys_res.setVisibility(View.VISIBLE);
                table_sys_res.setVisibility(View.VISIBLE);
                generateTable2(progress);
                mListener.onFragmentSend(XRoots);
            }
        });


        return v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public void generateTable (int N, boolean rand){
        table_sys_coef.removeAllViews();
        table_sys_coef.setColumnCount(N+1);
        table_sys_coef.setRowCount(N);
        cells = new LinearLayout[N][N+1];
        values = new EditText[N][N+1];
        for (int i = 0; i < N; i++){
            for (int j = 0; j < N+1; j++){
                int check = j;
                boolean flag = true;
                if (check == N)
                    flag = false;
                generateCell(i, j, flag, N, rand);
            }
        }
    }

    public void generateCell (int row, int col, boolean flag, int N, boolean rand){
        int widthLay = 800/N;
        int heightLay = 200;
        Random random = new Random();
        cells[row][col] = new LinearLayout(getActivity());
        cells[row][col].setOrientation(LinearLayout.HORIZONTAL);
        cells[row][col].setLayoutParams(new LinearLayout.LayoutParams(widthLay, heightLay));
        values[row][col] = new EditText(getActivity());
        TextView text = new TextView(getActivity());
        NumberFormat formatter = NumberFormat.getInstance(Locale.UK);
        formatter.setMaximumFractionDigits(2);
        text.setTextColor(Color.BLACK);
        if (!flag){
            text.setText("=");
            text.setLayoutParams(new LinearLayout.LayoutParams((int)(widthLay*0.1), heightLay));
            values[row][col].setHint("b"+row);
            values[row][col].setLayoutParams(new LinearLayout.LayoutParams((int)(widthLay*0.9), heightLay));
            if (rand){
                values[row][col].setText(formatter.format(((random.nextDouble()*2)-1)*30));
            }
            cells[row][col].addView(text);
            cells[row][col].addView(values[row][col]);
        }
        else{
            values[row][col].setHint("a"+col);
            values[row][col].setLayoutParams(new LinearLayout.LayoutParams((int)(widthLay*0.6), heightLay));
            text.setText("*x"+col+"+");
            text.setLayoutParams(new LinearLayout.LayoutParams((int)(widthLay*0.4), heightLay));
            if (rand){
                values[row][col].setText(formatter.format(((random.nextDouble()*2)-1)*30));
            }
            cells[row][col].addView(values[row][col]);
            cells[row][col].addView(text);
        }
        table_sys_coef.addView(cells[row][col]);
    }

    public void generateTable2 (int N){
        table_sys_res.removeAllViews();
        table_sys_res.setColumnCount(2);
        table_sys_res.setRowCount(N);
        cells2 = new LinearLayout[N][2];
        values2 = new EditText[N];
        for (int i = 0; i < N; i++){
            for (int j = 0; j < 2; j++){
                generateCell2(i, j);
            }
        }
    }

    public void generateCell2 (int row, int col){
        int heightLay = 200;
        cells2[row][col] = new LinearLayout(getActivity());
        cells2[row][col].setOrientation(LinearLayout.HORIZONTAL);
        if (col == 0) {
            cells2[row][col].setLayoutParams(new LinearLayout.LayoutParams(100, heightLay));
            TextView text = new TextView(getActivity());
            text.setTextColor(Color.BLACK);
            text.setGravity(Gravity.RELATIVE_HORIZONTAL_GRAVITY_MASK);
            text.setText("x" + row + "= ");
            text.setLayoutParams(new LinearLayout.LayoutParams(100, heightLay));
            cells2[row][col].addView(text);
        }
       else{
            cells2[row][col].setLayoutParams(new LinearLayout.LayoutParams(600, heightLay));
            values2[row] = new EditText(getActivity());
            values2[row].setKeyListener(null);
            values2[row].setText(String.valueOf(XRoots[row]));
            values2[row].setLayoutParams(new LinearLayout.LayoutParams(600, heightLay));
            cells2[row][col].addView(values2[row]);
        }
        table_sys_res.addView(cells2[row][col]);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {

        void onFragmentSend(double[] x);
    }
}
