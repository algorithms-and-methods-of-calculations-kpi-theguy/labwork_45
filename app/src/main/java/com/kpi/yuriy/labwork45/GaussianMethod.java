package com.kpi.yuriy.labwork45;


public class GaussianMethod {

    private int N;

    public GaussianMethod(int n) {
        N = n;
    }

    public double[] CalculationOfSystem (double[][] aMatrix, double[] bVector){
        int l = 0;
        int k = 0;
        for (k =  0; k < N; k++){
            double Amax = aMatrix[k][k];
            l = k;
            for (int i = k+1; i < N;i++){
                if(aMatrix[i][k]>Amax){
                    Amax = aMatrix[i][k];
                    l = i;
                }
            }
            if (l != k){
                double[] buff = new double[N];
                double z = 0;
                System.arraycopy(aMatrix[k], 0, buff, 0, N);
                z = bVector[k];
                System.arraycopy(aMatrix[l], 0, aMatrix[k], 0, N);
                bVector[k] = bVector[l];
                System.arraycopy(buff, 0, aMatrix[l], 0, N);
                bVector[l] = z;
            }
            double r = aMatrix[k][k];
            for (int j = k; j < N; j++){
                aMatrix[k][j] = aMatrix[k][j]/r;
            }
            bVector[k] = bVector[k]/r;
            for (int i = k+1; i < N; i++){
                double M = aMatrix[i][k];
                for (int j = k; j < N; j++){
                    aMatrix[i][j] = aMatrix[i][j] - M  * aMatrix[k][j];
                }
                bVector[i] = bVector[i] - M * bVector[k];
            }
        }
        double xVector[] = new double[N];
        xVector[N-1] = bVector[N-1];
        for (int i = N-2; i>-1; i--){
            double S = 0;
            for (int j = i+1; j < N; j++){
                S += aMatrix[i][j]*xVector[j];
            }
            xVector[i] = (bVector[i] - S);
        }
        return xVector;
    }
}
