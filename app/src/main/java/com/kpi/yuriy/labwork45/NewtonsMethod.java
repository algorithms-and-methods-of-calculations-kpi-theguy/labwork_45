package com.kpi.yuriy.labwork45;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;

import com.jjoe64.graphview.GraphView;

public class NewtonsMethod {

    private double e;
    private double pi = Math.PI;
    private int numIter = 0;

    public void setE(double e) {
        this.e = e;
    }

    public NewtonsMethod() {
    }

    public int getNumIter() {
        return numIter;
    }

    public double XCalculation (double a, double b) {
        double x = 0;
        numIter = 0;
        if (Math.abs(a-b) < e) {
            x = (a + b) / 2;
            return x;
        }
        if (singleFunction(b)*doubleFunction(b)<=0){
            double z = b;
            b = a;
            a = z;
        }
        while (true){
            x = b - (function(b)/singleFunction(b));
            numIter++;
            if (Math.abs(x-b)<e)
                break;
            b = x;
        }
        return x;
    }

    public double function (double x){
        return (Math.pow(Math.sin(x + pi/2), 2) - Math.pow(x,2)/4);
    }

    private double singleFunction (double x){
        return (2*Math.sin(pi/2+x)*Math.cos(pi/2+x)-(2*x)/4);
    }

    private double doubleFunction (double x){
        return (-2/4-2*Math.pow(Math.sin(pi/2+x),2)+2*Math.pow(Math.cos(pi/2+x),2));
    }
    public Bitmap getBipmapFromGraph (GraphView gv){
        Bitmap vBitmap = Bitmap.createBitmap(gv.getWidth(), gv.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(vBitmap){
            @Override
            public boolean isHardwareAccelerated(){return true;}
        };
        Drawable drawable = gv.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        gv.draw(canvas);
        return vBitmap;
    }
}





