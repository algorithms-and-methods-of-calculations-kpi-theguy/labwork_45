package com.kpi.yuriy.labwork45;

import android.Manifest;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.kpi.yuriy.labwork45.fragments.*;

import java.text.NumberFormat;
import java.util.Locale;

import static android.telephony.SmsManager.RESULT_ERROR_GENERIC_FAILURE;
import static android.telephony.SmsManager.RESULT_ERROR_NO_SERVICE;
import static android.telephony.SmsManager.RESULT_ERROR_NULL_PDU;
import static android.telephony.SmsManager.RESULT_ERROR_RADIO_OFF;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        NonLinearCalc.OnFragmentInteractionListener,
        SystemLinearCalc.OnFragmentInteractionListener{

    private Main fMain;
    private NonLinearCalc fNonLinCalc;
    private NonLinearGraph fNonLinGraph;
    private SystemLinearCalc fSysCalc;

    private BroadcastReceiver sbr;
    private BroadcastReceiver dbr;

    private View editView;
    private EditText phoneText;

    private double aCoef;
    private double bCoef;
    private double accurX;
    private double rootX;
    private double rootX2;
    private int iter;

    private double[] xVector;

    private final int PICKRESULT_OK = 8;
    private static final int REQUEST_SENT_SMS = 3;
    private static final int REQUEST_CONTACT = 2;
    private static final int REQUEST_WRITE_STORAGE = 1;
    private static final int REQUEST_EXTERNAL_STORAGE = 0;
    private static final String[] PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.SEND_SMS,
            Manifest.permission.READ_CONTACTS
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        FragmentTransaction fragT = getFragmentManager().beginTransaction();

        fMain = new Main();
        fNonLinCalc = new NonLinearCalc();
        fNonLinGraph = new NonLinearGraph();
        fSysCalc = new SystemLinearCalc();
        fragT.add(R.id.container, fMain).commit();

        verifyPermission(this);

        aCoef = 0;
        bCoef = 0;
        accurX = 0;
        rootX = 0;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_send_nonlinear){
            phoneDialog(false);
        }
        else if(id == R.id.action_send_system_linear) {
            phoneDialog(true);
        }
        else if (id == R.id.action_about){
            Snackbar.make(findViewById(R.id.container), "Author Yuriy Butskiy. KPI 2017, All rights reserved",
                    Snackbar.LENGTH_LONG).setAction("Action", null).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        FragmentTransaction fragT = getFragmentManager().beginTransaction();

        if (id == R.id.nav_main) {
            fragT.replace(R.id.container, fMain);
        } else if (id == R.id.nav_non_liear_calc) {
            fragT.replace(R.id.container, fNonLinCalc);
        } else if (id == R.id.nav_non_liear_graph) {
            fragT.replace(R.id.container, fNonLinGraph);
        } else if (id == R.id.nav_system_calc) {
            fragT.replace(R.id.container, fSysCalc);
        }
        fragT.commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void verifyPermission (Activity activity){
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE);
        int permission2 = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permission3 = ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_CONTACTS);
        int permission4 = ActivityCompat.checkSelfPermission(activity, Manifest.permission.SEND_SMS);

        if (permission != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(activity, PERMISSIONS, REQUEST_EXTERNAL_STORAGE);
        }

        if (permission2 != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(activity, PERMISSIONS, REQUEST_WRITE_STORAGE);
        }

        if (permission3 != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(activity, PERMISSIONS, REQUEST_CONTACT);
        }

        if (permission4 != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(activity, PERMISSIONS, REQUEST_SENT_SMS);
        }
    }

    public void phoneDialog(boolean flag){
        LayoutInflater inflater = this.getLayoutInflater();
        editView = inflater.inflate(R.layout.dialog_with_edit, null);
        AlertDialog.Builder dialogBuild = new AlertDialog.Builder(this);
        phoneText = (EditText) editView.findViewById(R.id.phoneText);
        ImageButton button_call = (ImageButton) editView.findViewById(R.id.button_call);
        button_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
                startActivityForResult(intent, PICKRESULT_OK);
            }
        });
        StringBuilder str = new StringBuilder();
        if (!flag){
            str.append("Entered equation: sin^2(x+pi/2)-x^2/4\n")
            .append("Ranges: a="+aCoef+"    b="+bCoef+"\n")
            .append("Result: x="+rootX+"  with accuracy="+accurX+"\n")
            .append("Number of iterations:"+iter);
            if (rootX2 != 0){
                str.append("\nAdditional root: x2="+rootX2);
            }
        }
        else{
            str.append("There was a system of linear equations. Result vector of X:\n");
            NumberFormat format = NumberFormat.getInstance(Locale.UK);
            format.setMaximumFractionDigits(3);
            if (xVector == null) {
                str.append("There wasn't calculation.");
            }else{
                for (int i = 0; i < xVector.length; i++) {
                    str.append("X" + i + " =" + format.format(xVector[i]) + "  ");
                }
            }
        }
        final String message = "Result:\n"+str;
        dialogBuild.setView(editView);
        dialogBuild.setTitle("Send SMS with result.");
        dialogBuild.setMessage("Enter or choose the phone number to sent result.\nYou entered this values:");
        dialogBuild.setMessage(message);
        dialogBuild.setPositiveButton("Send", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String phoneNumber = phoneText.getText().toString();
                sendSMS(phoneNumber, message);
            }
        });
        dialogBuild.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog a = dialogBuild.create();
        a.show();
    }

    public void sendSMS(String destination, String text){
        final String SEND = "SMS_SEND";
        final String DELIVERED = "SMS_DELIVERED";

        PendingIntent sendingPI = PendingIntent.getBroadcast(this, 0, new Intent(SEND), 0);
        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0, new Intent(DELIVERED), 0);

        sbr = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()){
                    case RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS send", Toast.LENGTH_SHORT).show();
                        break;
                    case RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(getBaseContext(), "SMS error. Generic Failure", Toast.LENGTH_SHORT).show();
                        break;
                    case RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(getBaseContext(), "SMS error. No Service", Toast.LENGTH_SHORT).show();
                        break;
                    case RESULT_ERROR_NULL_PDU:
                        Toast.makeText(getBaseContext(), "SMS error. Null PDU", Toast.LENGTH_SHORT).show();
                        break;
                    case RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(getBaseContext(), "SMS error. Radio Off", Toast.LENGTH_SHORT).show();
                        break;
                }
                try {
                    unregisterReceiver(sbr);
                }catch (IllegalArgumentException e){
                    Log.e("Error", "Wrong Receiver");
                }
            }
        };
        registerReceiver(sbr, new IntentFilter(SEND));

        dbr = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS delivered", Toast.LENGTH_SHORT).show();
                        break;
                    case RESULT_CANCELED:
                        Toast.makeText(getBaseContext(), "SMS not delivered", Toast.LENGTH_SHORT).show();
                        break;
                }
                unregisterReceiver(dbr);
            }
        };
        registerReceiver(dbr, new IntentFilter(DELIVERED));

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(destination, null, text, sendingPI, deliveredPI);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (resultCode == RESULT_OK) {
            if (requestCode == PICKRESULT_OK) {
                Uri uri = data.getData();
                String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER};
                Cursor c = getContentResolver().query(uri, projection, null, null, null);
                c.moveToFirst();
                int col = c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                phoneText.setText(c.getString(col));
            }
        }
        else{
            Toast.makeText(this, "Not correct contact!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onXCalculation(double x, double x2, double acc, int iter, int min, int max) {
        Bundle args = new Bundle();
        args.putDouble("XArgument", x);
        args.putDouble("X2Argument", x2);
        args.putInt("min", min);
        args.putInt("max", max);
        fNonLinGraph.setArguments(args);
        aCoef = min;
        bCoef = max;
        rootX = x;
        rootX2 = x2;
        accurX = acc;
        this.iter = iter;
    }

    @Override
    public void onFragmentSend(double[] x) {
        xVector = x;
    }
}
